FROM google/cloud-sdk:alpine
RUN apk --update add docker git openssl jq && \
    gcloud components install kubectl && \
    curl https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get | bash && helm init --client-only && \
    helm plugin install https://github.com/viglesiasce/helm-gcs.git
